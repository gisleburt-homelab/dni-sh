use yew::prelude::*;

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <div class="container">
            <div>
                <h1 class="h1">{ "Daniel & Indra" }</h1>
                <p class="lead">{ "Welcome!" }</p>
            </div>
        </div>
    }
}
