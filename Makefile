PHONY: %

install:
	rustup target add wasm32-unknown-unknown
	cargo install trunk
	cargo install wasm-bindgen-cli

install-tools: install
	cargo install sfz

build:
	rm docs/* || true
	trunk build
	cargo run --features ssr --bin ssg

build-release:
	trunk build --release
	cargo run --release --features ssr --bin ssg

serve: build
	sfz -r public

serve-release: build-release
	sfz -r public

preview:
	trunk serve index-no-hydrate.html

clean:
	cargo clean
	rm -r dist
	rm -r public
